var express = require('express');
var router = express.Router();
const fs = require('fs');
const cheerio = require('cheerio');
const got = require('got');
const request = require('request');

const vgmUrl= 'https://www.digitalocean.com/community/tutorials/how-to-scrape-a-website-with-node-js';

/* GET users listing. */
/*router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});*/

router.post('/scrap', (req, res) => {
 // const book = req.body;

  // Output the book to the console for debugging
 // console.log(book);
  //books.push(book);

  /*got(vgmUrl).then(response => {
    const $ = cheerio.load(response.body);
    console.log($);
  }).catch(err => {
    console.log(err);
  });*/
  request(vgmUrl, function (error, response, html) {
  if (!error && response.statusCode == 200) {
    const $ = cheerio.load(html);
    let result = [];
    let title = $("meta[property='og:title']").attr("content");
    let description = $("meta[property='og:description']").attr("content");
    let image = $("meta[property='og:image']").attr("content");

    result.push({"title":title,"description":description,"image":image});
    res.send(result);
   /* res.send($("meta[property='og:title']").attr("content"));
    res.send($("meta[property='og:description']").attr("content"));
    res.send($("meta[property='og:image']").attr("content"));*/
    //or 
    //console.log($("meta").get(1).attr("content")); // index of the meta tag
  }
});
  //res.send('Book is added to the database');
});

module.exports = router;
